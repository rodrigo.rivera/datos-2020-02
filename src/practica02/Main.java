package practica02;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Main mm = new Main();
        FileReader f = new FileReader("Input.txt"); //abre el archivo 
        BufferedReader b = new BufferedReader(f); // crea un bufer para  leer 

        int numeroMatriz = Integer.parseInt(b.readLine()); // lee la primera linea  
        for (int i = 0; i < numeroMatriz; i++) {   // lee el numero de matrices 
            String[] tam = b.readLine().split(" ");
            //int matrizTemp[][] = new int[Integer.parseInt(tam[0])][Integer.parseInt(tam[1])];
            Matriz m = new Matriz(Integer.parseInt(tam[0]), Integer.parseInt(tam[1]));
            for (int j = 0; j < Integer.parseInt(tam[0]); j++) {
                String[] renglo = b.readLine().split(" ");
                //System.out.println("" + renglo[0]+" - "+ renglo[1]+" -");
                double renglon[] = parser(renglo);
                m.setRenglon(j, renglon);
            }
            //mm.mostrar(m.getColumn(1));
            //m.mostrar();
        }

        b.close();
    }

    /**
     * cambia de tipo un arreglo de tipo String a double
     *
     * @param arreglo
     * @return
     */
    public static double[] parser(String[] arreglo) {
        double parse[] = new double[arreglo.length];
        String temp = "";
        for (int i = 0; i < parse.length; i++) {
            double d = Double.parseDouble(arreglo[i]);
            temp += d + " | ";
            parse[i] = d;
        }
        //System.out.println(temp);
        return parse;
    }
    
    public void mostrar( double array []){
        System.out.println(" otro renglon");
        String t ="";
        for (int i = 0; i < array.length; i++) {
            t+= array[i] +"|";
            
        }
        System.out.println(t);
    }
    

}
