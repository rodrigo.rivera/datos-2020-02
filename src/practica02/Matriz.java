package practica02;

public class Matriz implements Operable {

    private double[][] matriz;
    private int n;
    private int m;

    public void setRenglon(int pos, double matrix[]) {
        for (int j = 0; j < matrix.length; j++) {
            matriz[pos][j] = matrix[j];
        }
    }

    /**
     * se crea la trix vacia
     *
     * @param n tamaño de n
     * @param m tamaño de m
     */
    public Matriz(int n, int m) {
        if (n < 1 || m < 1) {
            throw new IllegalArgumentException("Dimension incorrecta");
        }
        if (n > 1000 || m > 1000) {
            throw new IllegalArgumentException("Dimension incorrecta");
        }
        matriz = new double[n][m];
        this.n = n;
        this.m = m;
    }

    /**
     * sobre escribe la matriz por una nueva
     *
     * @param mat
     */
    public Matriz(Matriz mat) {
        this.matriz = new double[mat.n][mat.m];
        for (int i = 0; i < mat.n; i++) { // rcolumnas 
            for (int j = 0; j < mat.m; j++) { // renglon 
                this.matriz[i][j] = mat.matriz[i][j];
            }
        }
        this.m = mat.m;
        this.n = mat.n;
    }

    /**
     * regresa el renglon de la matriz
     *
     * @param row
     * @return
     * @throws IllegalArgumentException
     */
    public double[] getRow(int row) throws IllegalArgumentException {
        if (row < 0 || row >= this.n) { // valida que no se pase de el tamaño 
            throw new IllegalArgumentException("Dimension incorrecta");
        } else {
            return this.matriz[row];
        }
    }

    /**
     * devulve la columna de la m,atriz
     *
     * @param column
     * @return
     * @throws IllegalArgumentException
     */
    public double[] getColumn(int column) throws IllegalArgumentException {
        if (column < 0 || column >= this.n) { // valida que no se pase de el tamaño 
            throw new IllegalArgumentException("Dimension incorrecta");
        } else {
            double[] temp = new double[m];
            String t1 = "";
            for (int i = 0; i < n; i++) {
                temp[i] = matriz[i][column];
                t1 += matriz[i][column] + " |";
            }
            //System.out.println(t1);
            return temp;
        }
    }

    public boolean equals(Matriz m) {
        return false;
    }

    public Matriz sum(Matriz m) throws UnsupportedOperationException {
        return null;
    }

    public Matriz scalarProduct(double c) throws UnsupportedOperationException {
        return null;
    }

    public Matriz product(Matriz m) throws UnsupportedOperationException {
        return null;
    }

    public double determinant2x2() throws UnsupportedOperationException {
        return 0.0;
    }

    public double determinant3x3() throws UnsupportedOperationException {
        return 0.0;
    }

    public Matriz transposed() throws UnsupportedOperationException {
        return null;
    }

    public void mostrar() {
        System.out.println("vamos a mostrar");
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                System.out.print(matriz[i][j] + " | ");

            }
            System.out.println("");

        }
    }
}
