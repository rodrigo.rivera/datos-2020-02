package practica02;

interface Operable {
	
	double[] getRow(int row) throws IllegalArgumentException,ArrayIndexOutOfBoundsException;
	double[] getColumn(int column) throws IllegalArgumentException,ArrayIndexOutOfBoundsException;
	boolean equals(Matriz m);
	Matriz sum(Matriz m) throws UnsupportedOperationException,NullPointerException;
	Matriz scalarProduct(double c) throws UnsupportedOperationException;
	Matriz product(Matriz m) throws UnsupportedOperationException,NullPointerException;
	double determinant3x3() throws UnsupportedOperationException;
	double determinant2x2() throws UnsupportedOperationException;
	Matriz transposed() throws UnsupportedOperationException;
}